class AddTypeInPatches < ActiveRecord::Migration
  def up
  	add_column :patches, :patch_type, :string
  end
  def down
  	remove_column :patches, :patch_type
  end
end
