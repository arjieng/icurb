class RenameTableResidentialSearchesToSearches < ActiveRecord::Migration
  def up
  	rename_table :residential_searches, :searches
  end
  def down
  	rename_table :searches, :residential_searches
  end
end
