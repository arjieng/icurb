class AddBusinessIdInBusiness < ActiveRecord::Migration
  def up
  	add_column :businesses, :business_id, :string
  end
  def down
  	remove_column :businesses, :business_id
  end
end