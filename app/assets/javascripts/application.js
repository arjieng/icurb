// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require_tree .
//= require jquery_ujs
//= require dataTables/jquery.dataTables
//= require dataTables.buttons.min
//= require pdfmake.min
//= require buttons.html5.min
//= require vfs_fonts
//= require bootstrap.min
//= require bootstrap-datepicker.min
//= require app.min
//= require components-date-time-pickers
//= require jquery.validate.min
//= require login.min
//= require form-validation
//= require ui-alerts-api
//= require bootstrap-select
//= require components-bootstrap-select

