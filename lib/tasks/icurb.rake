task update_person: :environment do
	log = Log.where("search_type = 'individual'").last
    zips = Zipcode.where("id >= ?", log.zipcode)
    @zip_id = nil
    @api_offset = log.api_offset.to_i
    begin
      offset = log.api_offset.to_i
      zips.each do |zip|
      	puts "#{zip.code} starting"
        @zip_id = zip.id
        count = JSON.parse(RestClient.post("https://api.infoconnect.com/v1/people/count?apikey=5GZ7uheIvWRH5pGKNJGnYEJBzr7dRzo3", { "PostalCode": [zip.code.to_s] }.to_json, { content_type: :json, accept: :json }))
        (1..(count["MatchCount"].to_f / 100).ceil).each do |f|
          persons = JSON.parse(RestClient.post("https://api.infoconnect.com/v1/people/search?apikey=5GZ7uheIvWRH5pGKNJGnYEJBzr7dRzo3", { "PostalCode": [zip.code.to_s], "Limit": 100, "Offset": offset, "Fields": ["LastName", "FirstName", "MiddleInitial", "Phone", "Address", "City", "StateProvince", "PostalCode", "Location", "AgeRange", "IncomeRange", "CreditCardRevolverScore"] }.to_json, { content_type: :json, accept: :json }))
          @api_offset += persons.count.to_i
          persons.each do |person|
            if person["Location"].present?
              if Location.exists? property_id: person["Id"]
                location = Location.where("property_id = ?", person["Id"])
                location[0].update_attributes(full_name: "#{person["LastName"]}, #{person["FirstName"]} #{person["MiddleInitial"].present? ? person["MiddleInitial"] : nil }", address: person["Address"], city: person["City"], state: person["StateProvince"], zip: person["PostalCode"], phone: person["Phone"], location_point: "(#{person["Location"]["Latitude"]}, #{person["Location"]["Longitude"]})", latitude: person["Location"]["Latitude"], longitude: person["Location"]["Longitude"], age: person["AgeRange"], credit_score: person["CreditCardRevolverScore"], income: person["IncomeRange"])
              	puts "Updated"
              else
                Location.create(property_id: person["Id"], full_name: "#{person["LastName"]}, #{person["FirstName"]} #{person["MiddleInitial"]} #{person["MiddleInitial"].present? ? person["MiddleInitial"] : nil}", address: person["Address"], city: person["City"], state: person["StateProvince"], zip: person["PostalCode"], phone: person["Phone"], location_point: "(#{person["Location"]["Latitude"]}, #{person["Location"]["Longitude"]})", latitude: person["Location"]["Latitude"], longitude: person["Location"]["Longitude"], age: person["AgeRange"], credit_score: person["CreditCardRevolverScore"], income: person["IncomeRange"])
              	puts "Added"
              end
            end
          end
          offset += persons.count.to_i
          puts "page turned"
        end
        offset = 0
        @api_offset = 0
      end
    rescue Exception => e
      Log.create(zipcode: @zip_id, api_offset: @api_offset, search_type: "individual")
      puts e
    end
end

task update_businesses: :environment do
	log = Log.where("search_type = 'business'").last
	zips = Zipcode.where("id >= ?", log.zipcode)
    zip_id = nil
    api_offset = log.api_offset.to_i
    begin
    	offset = log.api_offset.to_i
    	zips.each do |zip|
	      	puts "#{zip.code} starting"
	      	zip_id = zip.id
			count = JSON.parse(RestClient.post("https://api.infoconnect.com/v1/companies/count?apikey=5GZ7uheIvWRH5pGKNJGnYEJBzr7dRzo3", { "PostalCode": [zip.code.to_s] }.to_json, { content_type: :json, accept: :json }))
    		(1..(count["MatchCount"].to_f / 100).ceil).each do |f|
				businesses = JSON.parse(RestClient.post("https://api.infoconnect.com/v1/companies/search?apikey=5GZ7uheIvWRH5pGKNJGnYEJBzr7dRzo3", { "PostalCode": [zip.code.to_s], "Limit": 100, "Offset": offset, "Fields": ["CompanyName","YearEstablished", "LastName", "FirstName", "Phone", "Address", "LocationSalesVolumeActual", "LocationEmployeesSizeActual", "City", "StateProvince", "PostalCode", "Location", "EIN1", "BusinessType", "PrimarySic", "PrimaryNaics", "Title"] }.to_json, { content_type: :json, accept: :json }))
				api_offset += businesses.count.to_i
				businesses.each do |business|
					if business["Location"].present?
						if Business.exists? business_id: business["Id"]
							business = Business.where("business_id = ?", business["Id"])
							business[0].update_attributes(business_name: business["CompanyName"], business_owner: business["FirstName"].present? && business["LastName"].present? ? "#{business["LastName"]}, #{business["FirstName"]}" : nil, phone_number: business["Phone"], address: business["Address"], revenue: business["LocationSalesVolumeActual"], no_of_employees: business["LocationEmployeesSizeActual"], start_of_business: business["YearEastablished"].present? ? business["YearEastablished"] : nil, city: business["City"], state: business["StateProvince"], zip: business["PostalCode"], business_point: "(#{business["Location"]["Latitude"]}, #{business["Location"]["Longitude"]})", latitude: business["Location"]["Latitude"], longitude: business["Location"]["Longitude"], ein1: business["EIN1"], sic: business["PrimarySic"], naics: business["PrimaryNaics"], primary_contact_title: business["Title"])
							puts "#{counter}. updated"
						else
							Business.create(business_name: business["CompanyName"], business_owner: business["FirstName"].present? && business["LastName"].present? ? "#{business["LastName"]}, #{business["FirstName"]}" : nil, phone_number: business["Phone"], address: business["Address"], revenue: business["LocationSalesVolumeActual"], no_of_employees: business["LocationEmployeesSizeActual"], start_of_business: business["YearEastablished"].present? ? business["YearEastablished"] : nil, city: business["City"], state: business["StateProvince"], zip: business["PostalCode"], business_point: "(#{business["Location"]["Latitude"]}, #{business["Location"]["Longitude"]})", latitude: business["Location"]["Latitude"], longitude: business["Location"]["Longitude"], business_id: business["Id"], ein1: business["EIN1"], sic: business["PrimarySic"], naics: business["PrimaryNaics"], primary_contact_title: business["Title"] )
							puts "#{counter}. created"
						end
					end
					counter += 1
				end
				offset += businesses.count.to_i
          		puts "page turned"
    		end
    		offset = 0
    		api_offset = 0
    	end
    rescue Exception => e
	  Log.create(zipcode: zip_id, api_offset: api_offset, search_type: "business")
      puts e
    end
end

task onboard_api_business: :environment do
	zips = Zipcode.all
	zips.each do |zip|
		link = "https://search.onboard-apis.com/poisearch/v2.0.0/poi/geography?PostalCodeKey=#{zip.code}&SearchDistance=10000&RecordLimit=100"
		begin
			businesses = JSON.parse(RestClient.get(link, headers = { Accept: :json, apikey: "9d078487e223b1c4d54c3f3a3f628803" }))
			businesses["response"]["result"]["item"].each do |business|
				if Business.exists?(business_name: business["name"], state: business["state"], address: business["street"], city: business["city"], zip: business["zip_code"])
					business_name.update_attributes(business_name: business["name"], phone_number: business["phone"], address: address["street"], city: business["city"], state: businesses["state"], zip: business["zip_code"], latitude: business["geo_latitude"], longitude: business["geo_longitude"], business_point: "(#{business["geo_latitude"]}, #{business["geo_longitude"]})", business_type: business["business_category"])
					puts "update by address"
				else
					Business.create(business_name: business["name"], business_owner: nil, phone_number: business["phone"], address: address["street"], revenue: nil, no_of_employees: nil, start_of_business: nil, city: business["city"], state: businesses["state"], zip: business["zip_code"], latitude: business["geo_latitude"], longitude: business["geo_longitude"], business_point: "(#{business["geo_latitude"]}, #{business["geo_longitude"]})", business_id: business["ob_id"], ein1: nil, business_type: business["business_category"], sic: nil, naics: nil, primary_contact_title: nil)
					puts "added"
				end
			end
		rescue Exception => e
			puts e
		end
	end
end

task onboard_api_homeowner: :environment do
	zips = Zipcode.all
	zips.each do |zip|
		puts "#{zip.code}"
		page = 1
		count = 1
		while true
			begin
				link = "https://search.onboard-apis.com/propertyapi/v1.0.0/property/address?postalcode=#{zip.code}&page=#{page}&pagesize=10000"
				properties = JSON.parse(RestClient.get(link, headers = { Accept: :json, apikey: "9d078487e223b1c4d54c3f3a3f628803" }))
			  	puts properties["property"].count
			    properties["property"].each do |property|
			      identifier = property["identifier"]
			      homeowner = (Crack::XML.parse(RestClient.get("https://property.melissadata.net/v3/REST/Service.svc/doLookup?id=MD6HZ55cmVvEdmOQVv063f**&fips=#{identifier["fips"]}&apn=#{identifier["apn"]}")))["ResponseArray"]
			      puts homeowner["ResponseResult"]["Code"].nil?
			      if homeowner["ResponseResult"]["Code"].nil?
			      	homeowner = homeowner["Record"]
			      	address = homeowner["PropertyAddress"]
			      	if Location.exists?(address: address["Address"], city: address["City"], state: address["State"], zip: address["Zip"])
			      		location = (Location.where(address: address["Address"], city: address["City"], state: address["State"], zip: address["Zip"]))[0]
	      				location.update_attributes(full_name: homeowner["Owner"]["Name"], address: property["address"]["line1"], city: property["address"]["locality"], state: property["address"]["countrySubd"], zip: property["address"]["postal1"], phone: homeowner["Owner"]["Phone"], latitude: property["location"]["latitude"], longitude: property["location"]["longitude"], location_point: "(#{property["location"]["latitude"]}, #{property["location"]["longitude"]})", age: nil, credit_score: nil, income: nil)
		      			puts "#{count} update by address"
		      		else
	      				Location.create(full_name: homeowner["Owner"]["Name"], address: property["address"]["line1"], city: property["address"]["locality"], state: property["address"]["countrySubd"], zip: property["address"]["postal1"], phone: homeowner["Owner"]["Phone"], latitude: property["location"]["latitude"], longitude: property["location"]["longitude"], location_point: "(#{property["location"]["latitude"]}, #{property["location"]["longitude"]})", property_id: identifier["obPropId"], age: nil, credit_score: nil, income: nil)
	      				puts "#{count} Added"
		      		end
			      end
			      count += 1
			    end
				break if ((properties["status"]["total"].to_f / 10000).ceil == page )
				puts "page turned"
			rescue Exception => e
				break
				puts e
			end
		end
	end
end



# pagination = true
# page = 1
# while pagination == true
# 	properties = JSON.parse(HTTP.headers(apikey: "fb13b6fd4f5f29c4d70c7bea325c0369", accept: "application/json").post("https://search.onboard-apis.com/propertyapi/v1.0.0/property/address", params: { postalcode: 77042, page: page, pagesize: 1000, propertytype: :CONDOMINIUM } ).body)
	
# 	properties["property"].each do |property|
# 		point = Point.from_x_y_z(property["location"]["latitude"], property["location"]["longitude"], 0)
# 		if polygon.contains_point? point
# 			owner = JSON.parse(HTTP.headers(apikey: "d27f7ede2b3781185c5a365a96353d62", accept: "application/json").post("https://search.onboard-apis.com/propertyapi/v1.0.0/property/detailowner", params: { id: property["identifier"]["obPropId"] }))
			
# 			Search.create(user_id: current_user.id, patch_id: @patch.id, full_name: "Sample",
# 				address: "#{property["address"]["line1"]}, #{property["address"]["line2"]}", city: property["address"]["locality"], state: property["address"]["countrySubd"], zip: property["address"]["postal1"].to_i,
# 				phone: "0", location_point: "(#{property["location"]["latitude"].to_f}, #{property["location"]["longitude"].to_f})", location_id: property["identifier"]["obPropId"].to_s )
			
# 			@results.push {user_id: current_user.id, patch_id: @patch.id, full_name: "Sample",
# 				address: "#{property["address"]["line1"]}, #{property["address"]["line2"]}", city: property["address"]["locality"], state: property["address"]["countrySubd"], zip: property["address"]["postal1"].to_i,
# 				phone: "0", location_point: "(#{property["location"]["latitude"].to_f}, #{property["location"]["longitude"].to_f})", location_id: property["identifier"]["obPropId"].to_s}
			
# 		end
# 	end	

# 	if ((properties["status"]["total"].to_f / 1000).ceil == page)
# 		pagination = false
# 	end
# 	page += 1
# end