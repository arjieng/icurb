class RenameTableSearchesToResidentialSearch < ActiveRecord::Migration
  def up
  	rename_table :searches, :residential_searches
  end
  def down
  	rename_table :residential_searches, :searches
  end
end
