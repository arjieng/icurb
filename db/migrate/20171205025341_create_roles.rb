class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.string :name
      t.string :number
      t.float :amount
      t.integer :total_search
      t.timestamps null: false
    end
  end
end
