class Adminsdir::V1::Admins::RegistrationsController < Devise::RegistrationsController
  layout 'admin'
  def new
    super
  end

  def create
    super
  end
  def add_admin
  	build_resource(admin_params)
    if resource.save
      redirect_to root_path
    else
      redirect_to root_path
    end
  end
  private
  	def admin_params
  		params.require(:admin).permit(:email, :password, :first_name, :last_name, :is_active)
  	end
end