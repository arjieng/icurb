class AddTypeInLog < ActiveRecord::Migration
  def up
  	add_column :logs, :type, :string
  end
  def down
  	remove_column :logs, :type
  end
end
