Rails.application.routes.draw do
  devise_for :admins, path: 'adminsdir', controllers: {
    registrations: "adminsdir/v1/admins/registrations",
    sessions: "adminsdir/v1/admins/sessions"
    }, path_names: { sign_in: :login }

  devise_scope :admin do
    post 'adminsdir/add_admin' => "adminsdir/v1/admins/registrations#add_admin", as: :adminsdir_v1_admins_new_admin
  end  
  
  devise_for :users, path: '' , controllers: {
    registrations: "usersdir/v1/users/registrations",
    sessions: "usersdir/v1/users/sessions"
    }, path_names: { sign_up: :register }

  authenticated :admin do
    root "adminsdir/v1/admins/admins#index", as: :admin_authenticated_root
  end

  authenticated :user do
    root 'usersdir/v1/users/users#index', as: :authenticated_root
  end
  namespace :adminsdir do
    namespace :v1, path: '' do
      namespace :admins, path: '' do
        get 'add_admin' => "admins#new_admin"
        root 'admins#index'
        resources :users
        get 'change_status' => "users#change_status"
        get 'admin_change_status' => "admins#change_status"
        get "profile" => "admins#profile"
        post 'update_details' => "admins#update_details"
        patch 'update_password' => "admins#update_password"
        resources :admins

      end
    end
  end
  post 'new_card' => 'usersdir/v1/users/cards#add_new_card'
  patch 'update_password' => "usersdir/v1/users/users"
  post 'update_subscription' => "usersdir/v1/users/users#update_subscription"
  namespace :usersdir, path: '' do
    namespace :v1, path: '' do
      namespace :users, path: '' do
        get 'profile/:id' => 'users#show'
        post 'edit' => 'users#update_details'
        post 'exports' => 'users#exports'
        get 'exports' => 'users#exports'
        post 'filter_export' => 'users#filter_export'
        post 'clear_patches' => "users#clear_patches"
        resources :users, except: [:show] do
          get 'payments' => 'cards#new'
          resources :cards, except: [:new]
        end
      end
    end
  end

  
  get 'addpin' => 'welcome#addpin'
  get 'test' => 'welcome#stripetest'
  get 'pricing' => 'welcome#pricing'

  root 'welcome#index'
end
