class Usersdir::V1::Users::UsersController < ApplicationController
	before_action :complete_registration, except: [:incomplete]
	layout "users"
	def index
		@search_limit = ((Role.find(current_user.role_id)).total_search == (User.find(current_user.id)).search_count)
		@patches = Patch.where("user_id = ?", current_user.id)
	end
	def show
		@cards = current_user.cards.to_a
		@available = []
		@cards.each do |card|
			if card.is_active == true
				@active = card
			else
				@available.push card
			end
		end
		@searches = current_user.searches.select(:location_id, :full_name, :address, :city, :state, :zip, :phone).distinct
	end
	def clear_patches
		user_patches = current_user.patches
		user_patches.destroy_all
		render json: { destroyed: true }
	end
	def edit
		@role = Role.find(current_user.role_id)
	end
	def update_details
		if params[:commit] != "CANCEL"
			current_user.update_attributes user_params
			flash[:success] = "Succesfully updated Personal Info"
		end
		redirect_to edit_usersdir_v1_users_user_path(current_user.id)
	end

	def update_password
		if params[:commit] != "CANCEL"
			if current_user.update_with_password(pass_params)
				bypass_sign_in current_user
				flash[:success] = "Password successfully updated"
			else
				flash[:error] = "Password not saved"
			end
		end			
		redirect_to edit_usersdir_v1_users_user_path(current_user.id)
	end

	def update_subscription
		if params[:subscription][:role] != "#{params[:subscription][:sub_group].downcase}-#{params[:subscription][:sub_type].downcase}"
			subscription = Stripe::Subscription.retrieve(current_user.subscription_id)
			item_id = subscription.items.data[0].id
			items = [{
				id: item_id,
				plan: "#{params[:subscription][:sub_group].downcase}-#{params[:subscription][:sub_type].downcase}"
			}]
			subscription.items = items
			subscription.save

			role = Role.find_by_number("#{params[:subscription][:sub_group].downcase}-#{params[:subscription][:sub_type].downcase}")
			current_user.role_id =role.id
			current_user.save
		end
		redirect_to edit_usersdir_v1_users_user_path(current_user.id)
		
	end
	def exports
		@results = [[], [], []]
		if params[:sub_type].present?
			if params["sub_type"] == "Residential"
				vertices = JSON.parse(params[:geometry])
				geometry = [[]]
				vertices.each do |vertex|
					point = []
					vertex.to_a.each do |key, value|
						point.push value
					end
					geometry[0].push point
				end
				polygon = Polygon.from_coordinates(geometry ,4326)
				@patch = Patch.create(user_id: current_user.id, geometry: geometry, patch_type: "residential")
				locations = Location.all
				locations.each do |location|
					point = Point.from_x_y_z(location.location_point[0], location.location_point[1], 0)
					if polygon.contains_point? point
						Search.create(user_id: current_user.id, patch_id: @patch.id, full_name: location.full_name, address: location.address, city: location.city, state: location.state, zip: location.zip, phone: location.phone, location_point: location.location_point, location_id: location.id)
						@results[1].push location
					end
				end
				current_user.search_count.nil? ? current_user.search_count = 1 : current_user.search_count += 1
				current_user.save
				render json: { patch_id: @patch.id }
			else
				if params[:geometry].blank?
					business = {}
					if !params["city"].blank? && !params["state"].blank? && !params["zip"].blank? && !params["naics"].blank?
						business = Business.where("city = ? AND state = ? AND zip = ? AND naics = ?", params["city"], params["state"], params["zip"], params["naics"])
					elsif !params["city"] && !params["zip"] && !params["naics"]
						business = Business.where("city = ? AND zip = ? AND naics = ?", params["city"], params["zip"], params["naics"])
					elsif !params["state"] && !params["zip"] && !params["naics"]
						business = Business.where("state = ? AND zip = ? AND naics = ?", params["state"], params["zip"], params["naics"])
					elsif !params["city"] && !params["state"] && !params["zip"]
						business = Business.where("city = ? AND state = ? AND zip = ?", params["city"], params["state"], params["zip"])
					elsif !params["state"].blank? && !params["zip"].blank?
						business = Business.where("state = ? AND zip = ?", params["state"], params["zip"])
					elsif !params["state"].blank? && !params["naics"].blank?
						business = Business.where("state = ? AND naics = ?", params["state"], params["naics"])
					elsif !params["zip"].blank? && !params["naics"].blank?
						business = Business.where("naics = ? AND zip = ?", params["zip"], params["naics"])
					elsif !params["city"].blank? && !params["state"].blank?
						business = Business.where("city = ? AND state = ?", params["city"], params["state"])
					elsif !params["city"].blank? && !params["zip"].blank?
						business = Business.where("city = ? AND zip = ?", params["city"], params["zip"])
					elsif !params["city"].blank? && !params["naics"].blank?
						business = Business.where("city = ? AND naics = ?", params["city"], params["naics"])							
					elsif !params["state"].blank?
						business = Business.where("state = ?", params["state"])
					elsif !params["zip"].blank?
						business = Business.where("zip = ?", params["zip"])
					elsif !params["naics"].blank?
						business = Business.where("naics = ?", params["naics"])
					elsif !params["city"].blank?
						business = Business.where("city = ?", params["city"])
					end
					render json: business
				else
					vertices = JSON.parse(params[:geometry])
					geometry = [[]]
					vertices.each do |vertex|
						point = []
						vertex.to_a.each do |key, value|
							point.push value
						end
						geometry[0].push point
					end
					polygon = Polygon.from_coordinates(geometry ,4326)
					@patch = Patch.create(user_id: current_user.id, geometry: geometry, patch_type: "business")
					businesses = Business.all
					businesses.each do |business|
						point = Point.from_x_y_z(business.business_point[0], business.business_point[1], 0)
						if polygon.contains_point? point
							BusinessSearch.create(user_id: current_user.id, patch_id: @patch.id, business_name: business.business_name, business_owner: business.business_owner, phone_number: business.phone_number, address: business.address, city: business.city, state: business.state, zip: business.zip, revenue: business.revenue, no_of_employees: business.no_of_employees, start_of_business: business.start_of_business, latitude: business.latitude, longitude: business.longitude, business_point: business.business_point )
							@results[0].push business
						end
					end
					current_user.search_count.nil? ? current_user.search_count = 1 : current_user.search_count += 1
					current_user.save
					render json: { patch_id: @patch.id }
				end
			end
		elsif params[:patch_id].present? && !params[:filter].present?
			patch = Patch.find params[:patch_id]
			if patch.patch_type == "business"
				@results[0] = patch.business_searches
				@results[2] = { patch_id: patch.id }
				@results[2] = { patch_id: patch.id, rev_max: patch.business_searches.maximum(:revenue), rev_min: patch.business_searches.minimum(:revenue), no_of_employees_max: patch.business_searches.maximum(:no_of_employees), no_of_employees_min: patch.business_searches.minimum(:no_of_employees) }
			else
				@results[1] = patch.searches
				@results[2] = { patch_id: patch.id }
			end
		elsif params[:filter].present?
			@patch = Patch.find params["patch_id"]
			if @patch.patch_type == "business"
				if params["field"] == "employee"
					@results[0] = @patch.business_searches.where("no_of_employees BETWEEN  ? AND ?", params["min"], params["max"])
					@results[2] = { patch_id: @patch.id }
					@results[2] = { patch_id: @patch.id, rev_max: @patch.business_searches.maximum(:revenue), rev_min: @patch.business_searches.minimum(:revenue), no_of_employees_max: @patch.business_searches.maximum(:no_of_employees), no_of_employees_min: @patch.business_searches.minimum(:no_of_employees) }
				else
					@results[0] = @patch.business_searches.where("revenue BETWEEN  ? AND ?", params["min"], params["max"])
					@results[2] = { patch_id: @patch.id }
					@results[2] = { patch_id: @patch.id, rev_max: @patch.business_searches.maximum(:revenue), rev_min: @patch.business_searches.minimum(:revenue), no_of_employees_max: @patch.business_searches.maximum(:no_of_employees), no_of_employees_min: @patch.business_searches.minimum(:no_of_employees) }
				end
			end
		else
			@results[0] = current_user.business_searches.select(:business_name, :business_owner, :address, :city, :state, :zip, :phone_number, :revenue, :no_of_employees, :start_of_business).distinct
			@results[1] = current_user.searches.select(:location_id, :full_name, :address, :city, :state, :zip, :phone).distinct
		end
	end

	private
	  def complete_registration
	  	if current_user.is_active.zero?
	  		redirect_to usersdir_v1_users_user_payments_path(current_user.id)
	  	elsif current_user.is_active == 2
	  		sign_out current_user
	  		flash[:error] = "Account was deactivated"
	  		redirect_to root_path
  		end
	  end
	  def user_params
	  	params.require(:details).permit(:first_name, :last_name, :phone_number)
	  end
	  def pass_params
	  	params.require(:user).permit(:current_password, :password, :password_confirmation)
	  end
end
