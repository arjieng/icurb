class AddPropertyIdInLocation < ActiveRecord::Migration
  def up
  	add_column :locations, :property_id, :string
  end
  
  def down
  	remove_column :locations, :property_id
  end
end
