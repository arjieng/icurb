class RemoveSearchCountInSearches < ActiveRecord::Migration
  def up
  	remove_columns :searches, :search_count
  	add_column :users, :search_count, :integer
  end
  def down
  	add_column :searches, :search_count, :integer
  	remove_columns :users, :search_count
  end
end
