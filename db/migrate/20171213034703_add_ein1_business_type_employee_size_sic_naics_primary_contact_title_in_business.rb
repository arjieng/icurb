class AddEin1BusinessTypeEmployeeSizeSicNaicsPrimaryContactTitleInBusiness < ActiveRecord::Migration
  def up
  	add_column :businesses, :ein1, :string
  	add_column :businesses, :business_type, :string
  	add_column :businesses, :sic, :string
  	add_column :businesses, :naics, :string
  	add_column :businesses, :primary_contact_title, :string
  end
  def down
  	remove_column :businesses, :ein1
  	remove_column :businesses, :business_type
  	remove_column :businesses, :sic
  	remove_column :businesses, :naics
  	remove_column :businesses, :primary_contact_title
  end
end