class CreateLogs < ActiveRecord::Migration
  def change
    create_table :logs do |t|
      t.string :zipcode
      t.string :zip_offset
      
      t.string :api_offset
      t.timestamps null: false
    end
  end
end
