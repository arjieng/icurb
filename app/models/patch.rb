class Patch < ActiveRecord::Base
	belongs_to :user
	has_many :searches
	has_many :business_searches
end
