class RenameTypeToSearchTypeInLog < ActiveRecord::Migration
  def up
  	remove_column :logs, :type
  	add_column :logs, :search_type, :string
  end
  def down
  	add_column :logs, :type, :string
  	remove_column :logs, :search_type
  end
end
