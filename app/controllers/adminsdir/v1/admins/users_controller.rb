class Adminsdir::V1::Admins::UsersController < ApplicationController
	layout 'admin'
	before_action :log_in
	def index
		
	end
	def show
		@user = User.find params[:id]
		@role = Role.find @user.role_id
	end
	def change_status
		if !params[:status].nil?
			@user = User.find params[:id]
			@user.is_active = params[:status]
			@user.save
		end

		if !params[:index].nil?
			redirect_to adminsdir_v1_admins_root_path
		else
			redirect_to adminsdir_v1_admins_user_path(@user.id)
		end
	end
	private
	  def log_in
	  	if !admin_signed_in?
	  		redirect_to new_admin_session_path
	  	end
	  end
end
