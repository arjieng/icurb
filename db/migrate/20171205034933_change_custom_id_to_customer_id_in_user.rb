class ChangeCustomIdToCustomerIdInUser < ActiveRecord::Migration
  def up
  	rename_column :users, :custom_id, :customer_id
  end
  def down
  	rename_column :users, :customer_id, :custom_id
  end
end
