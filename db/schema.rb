# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171213060337) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone_number"
    t.string   "image"
    t.integer  "is_active"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "business_searches", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "patch_id"
    t.string   "business_name"
    t.string   "business_owner"
    t.string   "phone_number"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.integer  "zip"
    t.float    "revenue"
    t.integer  "no_of_employees"
    t.datetime "start_of_business"
    t.float    "latitude"
    t.float    "longitude"
    t.point    "business_point"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "businesses", force: :cascade do |t|
    t.string   "business_name"
    t.string   "business_owner"
    t.string   "phone_number"
    t.string   "address"
    t.float    "revenue"
    t.string   "no_of_employees"
    t.string   "start_of_business"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.float    "latitude"
    t.float    "longitude"
    t.point    "business_point"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "business_id"
    t.string   "ein1"
    t.string   "business_type"
    t.string   "sic"
    t.string   "naics"
    t.string   "primary_contact_title"
  end

  create_table "cards", force: :cascade do |t|
    t.string   "stripe_id"
    t.integer  "user_id"
    t.string   "last_4"
    t.string   "fingerprint"
    t.string   "brand"
    t.string   "exp_month"
    t.string   "exp_year"
    t.boolean  "is_active"
    t.string   "card_name"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "counties", force: :cascade do |t|
    t.integer  "state_id"
    t.string   "abbr"
    t.string   "name"
    t.string   "county_seat"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "counties", ["name"], name: "index_counties_on_name", using: :btree
  add_index "counties", ["state_id"], name: "index_counties_on_state_id", using: :btree

  create_table "locations", force: :cascade do |t|
    t.string   "full_name"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.integer  "zip"
    t.string   "phone"
    t.float    "latitude"
    t.float    "longitude"
    t.point    "location_point"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "property_id"
    t.string   "age"
    t.string   "credit_score"
    t.string   "income"
  end

  create_table "logs", force: :cascade do |t|
    t.string   "zipcode"
    t.string   "api_offset"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "search_type"
  end

  create_table "patches", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "geometry"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "patch_type"
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.string   "number"
    t.float    "amount"
    t.integer  "total_search"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "searches", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "patch_id"
    t.string   "full_name"
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.integer  "zip"
    t.string   "phone"
    t.point    "location_point"
    t.string   "location_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "states", force: :cascade do |t|
    t.string   "abbr",       limit: 2
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "states", ["abbr"], name: "index_states_on_abbr", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone_number"
    t.string   "image"
    t.string   "subscription"
    t.integer  "is_active"
    t.string   "customer_id"
    t.string   "role_id"
    t.string   "subscription_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "search_count"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "zipcodes", force: :cascade do |t|
    t.string   "code"
    t.string   "city"
    t.integer  "state_id"
    t.integer  "county_id"
    t.string   "area_code"
    t.decimal  "lat",        precision: 15, scale: 10
    t.decimal  "lon",        precision: 15, scale: 10
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "zipcodes", ["code"], name: "index_zipcodes_on_code", using: :btree
  add_index "zipcodes", ["county_id"], name: "index_zipcodes_on_county_id", using: :btree
  add_index "zipcodes", ["lat", "lon"], name: "index_zipcodes_on_lat_and_lon", using: :btree
  add_index "zipcodes", ["state_id"], name: "index_zipcodes_on_state_id", using: :btree

end
