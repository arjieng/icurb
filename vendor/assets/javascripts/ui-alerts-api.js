var UIAlertsApi = function () {

    var handleDemo = function() {        
        // $('#alert_show').click(function(){
            App.alert({
                container: "#alerts-container", // alerts parent container(by default placed after the page breadcrumbs)
                place: "append", // append or prepent in container 
                type: $("#type").val(),  // alert's type
                message: $("#message").val(),  // alert's message
                close: 0, // make alert closable
                reset: 0, // close all previouse alerts first
                focus: 1, // auto scroll to the alert after shown
                closeInSeconds: 5, // auto close after defined seconds
                icon: "warning" // put icon before the message
            });
        // });
    }

    return {

        //main function to initiate the module
        init: function () {
            handleDemo();
        }
    };

}();

jQuery(document).ready(function() {    
   UIAlertsApi.init();
});