class Adminsdir::V1::Admins::AdminsController < ApplicationController
	layout 'admin'
	before_action :log_in
	def index
		@admins = Admin.where("id != ?", current_admin.id)
		@users = User.all
	end
	def new_admin
		
	end
	def profile
		
	end
	def update_details
		if params[:commit] != "CANCEL"
			current_admin.update_attributes admin_params
			flash[:success] = "Succesfully updated Personal Info"
		end
		redirect_to adminsdir_v1_admins_profile_path
	end
	def update_password
		if params[:commit] != "CANCEL"
			if current_admin.update_with_password(password_params)
				bypass_sign_in current_admin
				flash[:success] = "Password successfully updated"
			else
				flash[:error] = "Password not saved"
			end
		end			
		redirect_to adminsdir_v1_admins_profile_path
	end
	def change_status
		if !params[:status].nil?
			@admin = Admin.find params[:id]
			@admin.is_active = params[:status]
			@admin.save
		end
		if !params[:index].nil?
			redirect_to adminsdir_v1_admins_root_path
		else
			redirect_to adminsdir_v1_admins_admin_path(@admin.id)
		end
	end
	def show
		@admin = Admin.find params[:id]
	end
	private
	  def log_in
	  	if !admin_signed_in?
	  		redirect_to new_admin_session_path
	  	elsif current_admin.is_active.zero?
	  		sign_out current_admin
	  		flash[:error] = "Account was deactivated"
	  		redirect_to new_admin_session_path
	  	end
	  end

	  def admin_params
	  	params.require(:details).permit(:first_name, :last_name, :phone_number)
	  end
	  def password_params
	  	params.require(:admin).permit(:current_password, :password, :password_confirmation)
	  end
end
