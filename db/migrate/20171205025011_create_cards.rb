class CreateCards < ActiveRecord::Migration
  def change
    create_table :cards do |t|
	  t.string :stripe_id
      t.belongs_to :user
      t.string :last_4
      t.string :fingerprint
      t.string :brand
      t.string :exp_month
      t.string :exp_year
      t.boolean :is_active
      t.string :card_name
      t.timestamps null: false
    end
  end
end
