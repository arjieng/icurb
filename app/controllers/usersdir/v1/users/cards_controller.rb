class Usersdir::V1::Users::CardsController < ApplicationController
	layout "users"
	def new
		@card = Card.new
	end
	def create
		begin
			customer = Stripe::Customer.retrieve(current_user.customer_id)
			sources = customer.sources.create(source: params[:stripe_token] )
			if customer
				card = Card.create( stripe_id: params[:stripe_token], user_id: current_user.id, last_4: sources.last4, fingerprint: sources.fingerprint, brand: sources.brand, exp_month: sources.exp_month, exp_year: sources.exp_year, is_active: 1, card_name: sources.name )
				if card
					@role = Role.find_by_number(current_user.subscription)
					@subscription = Stripe::Subscription.create(
					  :customer => current_user.customer_id,
					  :items => [
					    {
					      :plan => @role.number,
					    }
					  ]
					)
					current_user.is_active = 1
					current_user.role_id = @role.id
					current_user.subscription_id = @subscription.id
					current_user.save
					redirect_to root_path
				end
			end
		rescue Exception => e
			flash[:errors] = e.message
			redirect_to usersdir_v1_users_user_payments_path(current_user.id)
		end
	end

	def delete_card
		# cards = Stripe::Customer.retrieve("cus_BorNhUJ1kcGf1b").sources.all(object: :card)
		# cards.data.each do |card|
		# 	if card.fingerprint == params[""]
		# end
	end

	def add_new_card
		begin
			stripe_card = Stripe::Token.create( 
				:card => { 
					:number => params[:credit_card].to_s, 
					:exp_year => params[:expiry_date].split('-')[1].to_i, 
					:exp_month => params[:expiry_date].split('-')[0].to_i, 
					:cvc => params[:cvc].to_s,
					:name => params[:brand].to_s 
				} 
			)
			card = Card.find_by_fingerprint stripe_card.card.fingerprint
			if card.nil?
				Stripe::Customer.retrieve(current_user.customer_id).sources.create(source: stripe_card.id)
				card = Card.create( 
					stripe_id: stripe_card.id,
				 	user_id: current_user.id,
				 	last_4: stripe_card.card.last4, 
				 	fingerprint: stripe_card.card.fingerprint, 
				 	brand: stripe_card.card.brand, 
				 	exp_month: stripe_card.card.exp_month, 
				 	exp_year: stripe_card.card.exp_year, 
				 	is_active: 0, 
				 	card_name: stripe_card.card.name
				 )
			else
				flash[:error] = "This card already exists"
			end
			redirect_to usersdir_v1_users_profile_path(current_user.id)
		rescue Exception => e
			flash[:error] = e.message
			redirect_to usersdir_v1_users_profile_path(current_user.id)
		end
		
	end
end
