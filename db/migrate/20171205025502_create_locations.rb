class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :full_name
      t.string :address
      t.string :city
      t.string :state
      t.integer :zip
      t.string :phone
      t.float :latitude
      t.float :longitude
      t.point :location_point
      t.timestamps null: false
    end
  end
end
