module ApplicationHelper
	def javascript_include_action_specific_js
		javascript_include_tag "#{controller.controller_path.tr("/","_")}_#{controller.action_name}" if (File.exists?("#{Rails.root}/app/assets/javascripts/#{controller.controller_path.tr("/","_")}_#{controller.action_name}.js") || File.exists?("#{Rails.root}/app/assets/javascripts/#{controller.controller_path.tr("/","_")}_#{controller.action_name}.js.coffee"))
	end
end
