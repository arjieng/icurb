class CreateSearches < ActiveRecord::Migration
  def change
    create_table :searches do |t|
      t.belongs_to :user
      t.belongs_to :patch
      t.string :full_name
      t.string :address
      t.string :city
      t.string :state
      t.integer :zip
      t.string :phone
      t.point :location_point
      t.string :location_id
      t.timestamps null: false
    end
  end
end
