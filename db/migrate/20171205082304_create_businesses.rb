class CreateBusinesses < ActiveRecord::Migration
  def change
    create_table :businesses do |t|
      t.string :business_name
      t.string :business_owner
      t.string :phone_number
      t.string :address
      t.float :revenue
      t.string :no_of_employees
      t.string :start_of_business
      t.string :city
      t.string :state
      t.string :zip
      t.float :latitude
      t.float :longitude
      t.point :business_point
      t.timestamps null: false
    end
  end
end
