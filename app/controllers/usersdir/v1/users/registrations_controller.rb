class Usersdir::V1::Users::RegistrationsController < Devise::RegistrationsController
  def new
    super
  end

  def create
    super
  end
  
  private

  def sign_up_params
    params.require(:user).permit(:first_name, :last_name, :subscription, :is_active, :email, :password, :password_confirmation)
  end
end