class CreatePatches < ActiveRecord::Migration
  def change
    create_table :patches do |t|
      t.belongs_to :user
      t.string :geometry
      t.timestamps null: false
    end
  end
end
