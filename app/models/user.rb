class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  after_create :generate_customer_id
  has_many :cards
  has_many :patches
  has_many :searches
  has_many :business_searches
 private
	def generate_customer_id
		customer = Stripe::Customer.create(:email => self.email)
		self.customer_id = customer.id
		self.save
	end
end
