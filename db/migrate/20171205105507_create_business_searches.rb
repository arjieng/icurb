class CreateBusinessSearches < ActiveRecord::Migration
  def change
    create_table :business_searches do |t|
      t.belongs_to :user
      t.string :patch_id
      t.string :business_name
      t.string :business_owner
      t.string :phone_number
      t.string :address
      t.string :city
      t.string :state
      t.integer :zip
      t.float :revenue
      t.integer :no_of_employees
      t.datetime :start_of_business
      t.float :latitude
      t.float :longitude
      t.point :business_point
      t.timestamps null: false
    end
  end
end
