class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }
  # before_filter :force_www!

  private
    def force_www!
      if request.host[0..3] != "www." && Rails.env != "development" && request.host[0..3] != "172"
        redirect_to "#{request.protocol}www.#{request.host_with_port}#{request.fullpath}", :status => 301
      end
    end

    # Overwriting the sign_out redirect path method
    def after_sign_out_path_for(resource_or_scope)
      if resource_or_scope == :user
        root_path
      else
        adminsdir_v1_admins_root_path
      end
    end
end
