class RemoveZipOffsetInLogs < ActiveRecord::Migration
  def up
    remove_column :logs, :zip_offset
  end
  def down
    add_column :logs, :zip_offset, :string
  end
end
