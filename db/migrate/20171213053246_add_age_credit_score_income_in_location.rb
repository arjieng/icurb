class AddAgeCreditScoreIncomeInLocation < ActiveRecord::Migration
  def up
  	add_column :locations, :age, :string
  	add_column :locations, :credit_score, :string
  	add_column :locations, :income, :string
  end
  def down
  	remove_column :locations, :age
  	remove_column :locations, :credit_score
  	remove_column :locations, :income
  end
end
