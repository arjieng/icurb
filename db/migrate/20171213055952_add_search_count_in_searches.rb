class AddSearchCountInSearches < ActiveRecord::Migration
  def up
  	add_column :searches, :search_count, :integer
  end
  def down
  	remove_columns :searches, :search_count
  end
end
